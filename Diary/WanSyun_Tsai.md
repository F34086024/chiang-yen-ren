This diary file is written by Wan Syun Tsai E14073221 in the course Professional skills for engineering the third industrial revolution.

# 2020-9-8 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2020-9-15 #

* I was a little bit nervous about reporting in English.
* I realized that different people use different skill to collaborate with others.
* After watching Steven Pinker's TED talk, I was convinced the world becomes better in a rarely known manner.

# 2020-9-24 #

* I was really inspired by how to make a standard reference template.
* I do really appreciate teacher tell us where we can do better.
* I was a little bit tedious about the English bombaring of TED's speaker.

# 2020-10-08 #

* I learned how the economic work after watching the vedio.
* I was really impressed by the number of fake news.
* The vedio is too long for me to focus on.
* Don't have debt rise fsater than income.
* Do all that you can to raise your productivity.

# 2020-10-15 #

 * I should label the axis whenever I got the chart.
 * I need to totally understand the report I made.
 * Don't flash through the presentation cause others have no time to read.
 
# 2020-10-22 #

 * I learned that exercise can stimulate our brain.
 * Exercise at least three times a week.
 * Even go on a stroke can effect our mind.
 
# 2020-11-05 #
 
 * You need to accompany the depressed patient.
 * Suicide can't solve the problem but put stress on people around you.
 * Enbrace to the society can prevent you from depression.
 
# 2020-11-12 #

 * I was shocked being notified that we had to combine three groupe together and represent.
 * I met a little difficulties on the grid scale battery.
 
# 2020-11-19 #

 * We didn't sufficiently prepare for our topic.
 * We don't have to do the thing we don't want to according to common law.
 
# 2020-11-26 #

 * I think I need more data about our presentation.
 
# 2020-12-03 #

 * I did't prepare well for the presentation about grid scale batteries.
 * If we oppose the grid scale batteries we need to figure out the alternative.
 * Do no report the thing have no relation to the topic.
 
# 2020-12-10 #
 
 * I learned that I should use different template to fit with different screen ratio.
 * Our topic-biogeochemical flow is pretty hard compared to that of other groups.
 * We should be carefull not to transpass the planetary boundries.

# 2020-12-17 #

 * We have a debat bewween worker, enviromentalism and capitalism, and I am workwer.
 * The debate was not fierce at all.
 * I think our load of this class increase because of the individual diary.

# 2020-12-18 #
 * Successful and productive
 * Having a test, read some books, playing same online gmae.
 * I will go to sleep earily.
 
# 2020-12-19 #
 * Successful and unproductive
 * sleep a lot in the afternoon, playing same online gmae.
 * I will set a alarm in case over sleep.

# 2020-12-20 #
 * Successful and unproductive
 * I went home and bought a pillow, playing same online gmae.
 * I will go to sleep earily.
 
# 2020-12-21 #
 * Successful and productive
 * I almost skipped the test because of getting up late, biut I reod some books. 
 * I will get up earily.
 
# 2020-12-22 #
 * Successful and productive
 * Having a test, reading some books, playing same online gmae.
 * I will control my relax time.
 
# 2020-12-23 #
 * Unsuccessful and productive
 * I attend a speech but didn't focus on what he said.
 * I will pay attention next time.
 
# 2020-12-24 #
 * Successful and productive
 * I felt successful and productive because I have a lot of progress on my study.
 * I will go to sleep earily.
 
# 2020-12-25 #
 * Successful and productive
 * It's Friday, usually a lazy day for me, but I still finish the thing on schedule.
 * I will adjust my scheudle.

# 2020-12-26 #
 * Unsuccessful and unproductive
 * I went home and be a couch pptato.
 * I will break the TV.

# 2020-12-27 #
 * Unsuccessful and productive
 * Having a test, read some books, playing same online gmae,skipping a class.
 * I will attend the class.
 
# 2020-12-28 #
 * Successful and productive
 * I read a lot of book related to the final exam.
 * I will not choose too much class next semester.
 
# 2020-12-29 #
 * Successful and productive
 * I attended a conference today.
 * I will be ontime.

# 2020-12-30 #
 * Successful and productive
 * I read a lot of book related to the final exam.
 * I will sleep more to energize myself.