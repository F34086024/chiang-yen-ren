

This diary file is written by Justin Lin E14076588 in the course Professional skills for engineering the third industrial revolution.


# 2020-09-21 #

* The speech by Steven Pinker is really cool,because he use many datum to let us think about if the world getting better or not.
* Professor let us know there are five conflict behaviors.
* After wachting the TED by Steven Pinker,i realise that i can use datum or chart to make listener more understood when giving a speech.

# 2020-09-24 #

* Today teacher teach us how to improve the skill of giving a speech and making a PPT.
* Fake news surround us everywhere, we should know how to distinguish them.
* We might contact fake news from social media every day,we will be tricked if we don't investigate every information carefully.

# 2020-10-08 #

* We watch a video about ecocomic.
* Credit is the most important part of economy.
* Borrowing create cycle.
* I realise 3 main forces that drives the economy.
* Spending drives economy.

# 2020-10-18 #

* We watch two videos.
* First video is talking about the differences between nationalism and fascism.
* Facism says nation is the most beautiful thing in the world.
* Second video is a speech from a neo nazi leader's story.
* From a racist,the speaker becomes a Anti-racist.

# 2020-10-22 #

* We watch tow videos about  medical knowledge.
* Exercise is the most transformotive thing for brain.
* Hippocampus is about long term memory.
* Exercise not only can give you good mood,but also can protect your brain.
* Second video is talking about that prevention is better than cure.

# 2020-10-29 #

* We make a super group to give prsentation,it's a cool experience.
* From the presentation,we can know that soccer is a negative example exercise for our brains.
* We can't tell a depreesed person "just get over it".
* Second video is a Golden Gate Bridge officer's story about his successful and fail experience when helping suicidal people.
* We can listen to depressed people instead of giving them advices.

# 2020-11-05 #

* Today we know seven work group success factors 
* From the course we can improve our deficiency in seven qualitative thinking skills
* First video is a man uses simple trick but no one notice.
* In second video, data show that chasing happiness may make people unhappy.
* Four points to live more meaningfully are Belonging, Purpose, Transcendence and storytelling.

# 2020-11-12 #

* From group1's presentation and classmates' discussion, we can know that some people agree UBI and some people reject it.
* From group3's presentation, the current credit based monetary system isn't the best for society.
* Maxism might be good for reduce the gap between rich and poor.
* Maxisum is definitely not the substitute solution for current monetary because of the failed example such as China and Soviet Union.
* We watch a video about legal system.

# 2020-11-19 #
* We can know that marijuana is illegal in Taiwan, but it is legal in some states in America.
* Euthanasia is first become legal in Netherland.
* Most classmates think that Taiwan's election law is better than America's. 

# 2020-11-26 #
* We can know that Kenya has a UBI experiment.
* Professor made a experiment about cellphone addiction, so we need to put our cellphones on the desk in front of teacher.
* Kibbutz is a communist region in Israel.
* We need to have a decentralized finance system.
* We can know that Rome and USA have same problems,and these problems make them fall.

# 2020-11-30 #
* From different news source, a same event might have competely different view.
* China's newspaper express the conflict between Hong Kong and China with Nationalism.
* Australia's newspaper reports the same event with Liberalism.
* From first video, I know that there are nine planetary boundaries.
* There twelve social foundations.

# 2020-12-03 #
* Today's class is very different from other times because of the debate.
* We watch a video about the third industrial revolution.
* I think the boundary:Stratospheric ozone depletion is really important, because there is nobody want to be in danger when staying outside.
* We can eat vegetarian 3 days a week to reduce CO2 emission, and it is really not a big problem for me.
* Professor is really unwilling to eat vegetarian 3 days a week.

# 2020-12-17 Thu #
* A.Unsuccessful and productive.
* B.My presentaion is a little boring.
* C.Tomorrow will have a morning basketball training.

# 2020-12-18 Fri #
* A.Unsuccessful and unproductive.
* B.I didn't get up early to join training.
* C.Tomorrow I will have work in cram school.

# 2020-12-19 Sat #
* A.Successful and productive.
* B.I feel successful because I did a pretty good job in my work and feel productive in EV3 homework.
* C.Tomorrow will have to do more in EV3 homework.

# 2020-12-20 Sun #
* A.Successful and unproductive.
* B.I feel successful in my workout in gym and feel unproductive because our EV3 is not progressing.
* C.Tomorrow will have quiz in machine designing.

# 2020-12-21 Mon #
* A.Successful and unproductive.
* B.I feel successful because I finish machine designing quiz and finally go to the Composite Material Mechanic course.
* C.Tomorrow will have Control System Engineering course.

# 2020-12-22 Tue #
* A.Unsuccessful and unproductive.
* B.I feel unsuccessful and unproductive because machine designing is really boring and difficult.
* C.Tomorrow I will go to Tainan Art Museum Building 2 to appreciate exhibition.

# 2020-12-23 Wed #
* A.Successful and unproductive.
* B.I feel unproductive because my arrangement is delayed too much.
* C.Tomorrow we need to dress successful in the class tomorrow.

# 2020-12-24 Thu #

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I get bad grade in an exam . I was unproductive because I can't focus in class.
* C. I will be more efficient and more productive because I love Friday.
# 2020-12-25 Fri #
* A. Successful and productive.
* B. I felt successful because I learn so much in fluid mechanics.
* C. I will be productive because Saturday is the best time to study.
# 2020-12-26 Sat #
* A. Unsuccessful and productive
* B. I felt unsuccessful because my ev3 is still not good.
* C. I will have less time to study tomorrow and probably will be more unproductive because of the basketball team dinner par
# 2020-12-27 Sun #
* A. Successful and unproductive
* B. I felt successful because my ev3 can stand again.
* C. I will have time to study tomorrow and probably will be more productive.
# 2020-12-28 Mon #
* A. Successful and unproductive
* B. I felt successful because I learn many acknowledge from composite material.  But I was unproductive because I took a lot of time playing computer game.
* C. I will probably be more productive and more effective .
# 2020-12-29 Tue #
* A. Successful and productive
* B. I felt successful because I finally eat my favorite dumpling. I was productive because I use many rest time to study.
* C. I will probably be more productive and more effective.
# 2020-12-30 Wed #
* A. Successful and unproductive
* B. I felt successful because I finish my course project. I was unproductive because I waste so many time machine design.
* C. I will be more productive because I don't need to study machine design tomorrow.