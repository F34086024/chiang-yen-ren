# 2019-09-19

* I have never kept an english diary before, so I am a little bit excited about it. 
* I watched the lecture vedio again, for the reason that I didn't quite understand about the lecturer's standpoint, and why the problem "Is the world getting better or worse?" is so importanat for us. Though the lecturer said, it's not just a faith or perspective that someone in, it's a fact that indeed affects many people's feeling(to my understand), I think it's not always the same result when considering other aspects like emotion condition. Many people's emotion condition is getting worse than before. In my opinion, whether the world is getting better relies on which aspect we discuss. I wouldn't say which one is right definitely, and I think maybe it has nothing to with us. All I care are whether I'm doing good, and what makes me a better person.
* Last week, I was curious about how can we find a product which is produced by an exponential rate and then show us how the price go down. But through the lecture this Thursday, I realized which is a cumulative units produced.
* I was introduced the Wright's law and amazed by how can we pridict the price after a few years after. This is what I have never thought.

# 2019-09-26

* Only one group has tried to challenge the claim that Steven Pinker proposed, neither do I. They tried to inspect the data Steven Prinker had claimed. Though they finally think Steven Pinker is right in what he had claimed, I think they really did a great job.
* We have assistants to teach as substitute this week, and she really cared about where we cited our data, and our format of pptx which is in order to promote our presentation. She is really a dedicated substitute teacher.
* I used to consider myself as a person who can detect fake news. But when talked about our next task detecting fake news in fb, I had no idea. And now I can't help doubting myself whether I have been deceived for many years.><

# 2019-10-03

* I am sorry, this will be the last time I updated my diary so late. I will do it right after the class or before 24:00 on the same day of the class.
* It's stunning for me to think about who gives the money its value. Who plays the rule? Many people run after money, but when money has little value, like someone who control, what will be the end of those people who run after money?
* I used to be curious about why we get interest after depositting money in the banks, I understood what banks do was like invest on others. And Prof. Werner also gave a good explanation in the video.

# 2019-10-17

* Some groups give the presentation very well, and they try to investigate the data in Prof. Werne's claim.
* For me, I beleive what Prof. Werne said but I didn't take action to support or investigate what his said, that is bad.
* The second video really touched me. When I saw the title of extremism, it only occurs to me what objective factors would bring the extremism. Is it hatred, education, or poverty that brings extremism? End of the video, I realized incomprehension could also bring the extremism. It's inspiring.
* I regarded the man in second video as a hero. It's not easy to turn back from one's wrong path.

# 2019-10-24

* The first video is about claiming that excercise makes better of our life. I am not completely convinced by that. I came up with a hypothesis that exercise is not just an only way to improve our life. If it is true, then exercise seems not necessary very much.
* The second one is one trying to improve Health insurance, to my understanding. Briefly to say, government subsidizes doctors only when patients are getting better, otherwise, it doesn't. In other word, it depends on the result.
* Thus, how will the doctor do when encounters an unrecoverable patient? And I came up with a not funny joke that someone who is practicing mercy killing won't get any subsidization forever.
* I think presentor is giving advise out of kindness, but it may seem hard to carry out.
* Many problems arise from human morality, so instead of coming up with a seeminly complete system, I would recommend teaching public to be kind.

# 2019-10-31

* Today's lecturing time teacher taught us about typing errors, ppt typesetting and so on. Frankly speaking, I haven't heard that before, it's a precious lesson for me, indeed.
* I have ever met some friends who's in depression, and to the extent to commiting suicide. I think it is an important lesson to know about how to be with our friends. We could just listen, accompany, ask how they are fealing now, and even encourage them to seek professional help.
* Most important, we can make ourself more humble(I think most people hate seeing that, including someone who is depressed), kind, compassionate and not being too busy to have time with them. 

# 2019-11-07

* The first vedio seems showing that one's decision is easly manipulated. No matter things are right or false, it's someone's interpretation that matters.
* The second vedio is about forgiving. Our consensus about forgiving is not only is it good for people that harm you, but also you are treating yourself great since not let you unhappy all the time. And we discussed the miserable story classmate raised. I agree with a man that we shouldn't let forgiving end up our life. We should protect ourselves all the way.
* The third vedio is about someone telling four things that back up her life. I think find out something meaningful to all of us is important. I practices Falun Dafa that teach us truthfulness, compassion, forebearance, and it's my belief. I am lucky to have practiced Falun Dafa. But it has been persecuted in China for roughly twenty years, with cruel situation in China. All I hope is telling the truth happened in China, and it's meaningful, and what I need to do to improve the situation.

# 2019-11-21

* Today's videos are about the internet which is a virtual world that we are all easily addicted in.
* The first video mentioned that with the aid of Internet, facebook and Google is like an empire of behavior modification, where people change their mind or behavior due to the feedback of the social network. Human being on the Net is like having transactions with high frequency, and thus, are not like the one they used to be.
* He mentioned maybe it's the only solution that we trace back to the time that Internet is getting stronger, and charge a bit money, to my understanding.
* The second video tells us that Internet is like alcohol. When we have friends in front of us while we are watching cellphone, we are addicted to it. Well, I seems have that proplem.
* The third one is related to algorithms, but I don't quite understand. It's a little bit difficult to me.

# 2019-12-05

* Today's lecture is about planetary boundaries that I haven't heard of. And the distributed topic to our group is Atmospheric aerosol loading.
* Planetary boundaries are not cosmological concepts that I once thought. Instead, it diefines the environmental limits within which humanity can safely operate. I wondered how these 9 catogories of boudaries are set up. Because when we set up a frame, then we are easily limited especially our thoughts and cognition. How will it be when when other factors or other boundaries are also important but not considered in these boundaries while we just think not to operate beyond these 9 boundaries? SO our thoughts must be flexible when building or discussing these boundaries, not just only these 9 boundaries, in my opinion.

# 2019-12-26

* Presentation of the 15 questions.
* Our workshop is having brainstorming solutions for the questions presented today by individual groups, and we should best do that before the presenting day.

# 2020-01-02

* I thind today's topic is how to make things different. Like the first video we saw in the class is "What makes something go viral?".
* The second video is about educator encouraging students to find their voices. If it is related to politics, then "Teaching will always be a political act," said educator, Chaffee, is what we can't avoid.
* We also presented three concepts for solutions of the 15 questions, this reminds me how we could actually solve problems and to make changes to our worlds.
* So until next week, we should make a conbined group and impact a few people, actually as many as we could, and present that how many or how we have made the world different.

# 2020-01-09
* Today is the last class of the courses. I learn a lot from the course, especially in exposing to a english environment that make me feel not afraid when communitating with others.
* The first video we watched is about mathimatical methods for finding love. I am also eager to finding someone, haha. I'll take these methods.