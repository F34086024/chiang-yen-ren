# 2019-09-19
## Second Lesson
* After having the second lesson from this course, i am starting to like this course as in NCKU, there aren't many course that is fully lectured with english as the primary language.I am glad that i attend this course as it can train me in english speaking as it will help us to blend into a more international society. 
* By seeing the TED Talk that is introduced in the lecture, i think every coin have its heads and tails, also in every situation have a good and bad situation.For example, the earth that we live in. There's no doubt that the world is getting worse in the pollution, murdering case and etc.But look on the bright side, there are lot's and lot's of improvement in many ways, such as the safety of aircraft, the technology nowadys and many more.
* Also, through this lecture, i learned about this new software that i am currently using to write this diary. I think that it's is a new and fresh way to let us learn and dive into the modern society as there are numerous other new software developing throughtout the time.

# 2019-09-26
## Third Lesson
* Today lesson has given me so much insight in the problems of the news nowadays. Media uses click-bait and false news to cover up the situation that is really happening right now. So, whenever we see a news, we couldn't jump into believing that this news giving us the real information. But we should really dug into researh whether the news is reliable or it is another one of the fake news.
* We also learned from the TED Talk some method indicating whether the news is fake or real. The presenter said that fake news have some of the common characteristic. The characteristic of the fake news is: Click-bait, Over dramatic and too emotional. 
* Lastly, from the lecturer, i learned that the importance of copyright and some key when you are prepairing for a presentation. When we found an information, whether it is a piece of article or just a picture, we need to put the reference link in our presentation as this is not our property. Second, when we have a template for the presentation, we shouldn't overlap the design of our template as to respect the template. 

# 2019-10-03
## Fourth Lesson
* Today lesson have given me an understanding about how the banking system works.
* As a student from Mechanical Department, we have limited access of knowldege about how the economic works. From the video, it states that the economy is driven transaction from one people to another. As the transaction chain grows, it become larger and larger and eventually becomes a market.
* For the borrower and lender part, it states that when the borrower lends money from the lender, to maintain the liability for the borrower, debt is created to ensure that the lender will get his/her money back
* From the TED Talk, the lecturer use a very interesting method to let his children know the importance of using the money wisely. The method that he used is by playing a well-known board game Monopoly, and instead of fake money, he take out his saving and used real money as the asset for every player in the game, and the winner of the game will get an amount of price as the reward. By doing this, it will encourage them to think more wisely on what to spend and invest on to get the most advantage.

# 2019-10-17
## Fifth Lesson
* Today lesson is very fruitful as it helps me know about what the world is really happening right now, as the facism people exist nowadys.
* From the video from Yuval Voah Harari, it helps me know about the difference between facism and nationalism.

# 2019-10-24
## Sixth Lesson
* Path of gaining knowledge ---- learning about well-justified belief truth
* The way to communicate and discuss in a larger group
* Having a healthy lifestyle benifit us in many ways
* It is better to detect disease before it is too late

# 2019-10-31
## Seventh Lesson
* We need to strive to let our task to be perfect in every way, no matter it is in our daily life or our task and assignment.
* Depression cannot be ignored as it is a very serious problem. 
* What to do when we faced people with depression.
* There's no need to be scared about when in a depression state.
* Don't choose the method of suicide as a way to run away from the problems you are facing. 
* Be brave to have helped from others. 

# 2019-11-07
## Eight lesson
* Today we learn about the difference about happiness and fulfilment.
* We can easily be fooled and manipulate. For example the �Choice Blindness"
* Sometime we prefer choosing by the appearance than the reason for choosing it. 
* Forgiving give us better life, as being in misery and vengeful is not an option for having a fruitful life.
* Ideal job and success doesn't equal to happiness.
* We should pursure a more meaningful life than chasing happiness.
* Four pillars if meaningful life:  *Belongings 
									*Purpose
									*Transcendence
									*Storytelling 
# 2019-11-14
## Ninth Lesson
* Today definitely is the best lesson as i attended this course so far, as it breaks the traditional thinking about how the law in the society works.
* Learned some vocabulary in a "legal" meaning. 
* I have learned the most valuable advice: "Don't be prisoned by your own mind, learn to be free by think freely"
* We must learn our own rights as a people, not to be scared to accused other about own rights.
* Learn to protect our own personal information.
* Get to know that some public servant nowadys are corrupted with the power they don't own.
* Not letting "legalised" people to trespass your own property, but with a legalised proof.

# 2019-11-21
## Tenth Lesson
* Today we talked about what the millennials are facing nowadays.
* Millenials don't have coping mechanism to stress ---Cause of depression and suicide rate increase.
* Lower self-esteem.
* We are constantly using the social media, one of the reason for depression
* Lack the ability of being patience.
* Increase suicide rate, increase drug usage.
* We are currently living in a environment which we demand to sees things in a short term time.
* We are constantly changing and altering our algorithm in life to be "successful"

# 2019-11-28
# Eleventh lesson
* As the times pass by, the media has been enlarged from a few through the help of internet and social media.
* We can't always trust the information in the media as maybe they are "polished" by the media.
* We should check for the latest information by at several point in a day for full information.
* Checking various source for the news to check for the reliability of the information

# 2019-12-05
## Twelfth Lesson
* We know about the media in different country have different prespective about the same news
* Some of them are more nationalistic, while some of them are more libertarian. 
* Know about the importance of planetery boundaries.
* Each planetery boundaries have different importance towards the people living in the Earth. 
* We should exceed the social foundation but not to over exceed the ecological ceiling.

# 2019-12-13
## Thirteenth lesson
* Today class is very interesting as we have a lot of debate about which is the most importatnt planatery boundary.
* We cannot underestimate the power of deforestation as it birngs critical consequences.
* Maybe a small little problem that we think might bring catostrophic outcome.
* We have a lot of fun debating and learn alot of information about other planatery boundary during this period.

# 2019-12-19 
## Fourteenth lesson
* In today class, we have a more deep insight about what is happening in industrial 3.0
* From today video, we learn some few step to help us stay away from procrastination. 
* Tips to be more productive: (i) Take a stroll if you are not motivated to do your task.
							  (ii) Have a clean and tidy environment helps in motivate us to be more productivity.
* We are given a task to make a diary daily to describe how successful and productive our day is.

# Daily diary
## 2019-12-19 (Thursday)
	A. Successful and productive day
	B. When to have a exercise in the gym, felt more energetic.
	C. Cleaned my messy room after being suggested from the video.
	D. I will study and be more productive tomorrow by going to the library.
## 2019-12-20 (Friday)
	Not so productive day
	A. Not so productive as i expected.
	B. Because of the present of the phone, makes me distract.
	C. Failed to go to the library because of working.
	D. Stay away from phone to have more productivity.
## 2019-12-21 (Saturday)
	It is a productive day
	A. More productive than yesterday. 
	B. But still get distracted by my phone all the time when i see it.
	C. I will try to stop my using of my phone while i am studying.
## 2019-12-22 (Sunday)
	It is a productive day
	A. Finally went to the library to study.
	B. Today is a very productive day as i use less of my phone.
	C. But in the library studying makes me tired.
	D. Tomorrow will go out and have a walk to relax myself.
	
## 2019-12-23 (Monday)
	A.Productive day
	B.Go to work and also have time to have my revision for my homework.
	
## 2019-12-24 (Tuesday)
	A.Not a very productive day 
	B.Because of the festive season, makes me more lazy
	
## 2019-12-25(wednsday)
	A.A productive day
	B. Go to work and have my assignment, presentation done.
	
Five rules to have a more productive day
* Leave away from your phone (distraction)
* Have a plan for the following day
* Try to stick to the plan and not altering it.
* Go to have a walk and relax your mind
* Don't make your target too hard to accomplish.

## 2019-12-26
## Fifteenth Lesson
* Learned about alot of problems we are facing nowaday
* Gold is the currency of the future.
* US Dolloar has lost its priviledge in the economical chain

## 2020-01-02
## Sixteen Lesson
* Know about our classmate diary about successful or unsuccessful day.
* Get to know about how to make a change from the video of TED Talk.
* Get to know about the solution for the problem that the professor let us brainstorm for.