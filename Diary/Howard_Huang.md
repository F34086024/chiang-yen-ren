This diary file is written by Howard, Kuan-Hao Huang (N18044010) in the course of Professional Skills for Engineering the Third Industrial Revolution.

# 2019-09-12

* Course information is introduced by Prof. Nordling, Ph.D.
* The pretest of this course is a little bit hard to me.
* Announcement of the mini problem by making 3 mins presentation next week.

# 2019-09-19

* Learn how to deal with the conflict views.
* Learn how to use distributed version control system (Git) to submit the diary after each lecture.
* Professor introduces the disruptive innovation and exponential growth by giving us some examples (Ford vs. Tesla)(Wright�s law).
* Pick up one of the claims based on TED talk (by Steven Pinker) as group's mini problem next week and try to find evidence against or support the claim.

# 2019-09-26

* Reminder about how to write diary after each lecture.
* Presentation of mini problem 2: Response to a statement of claim from Steven Pinker.
* TED Talk: How to seek truth in the era of fake news(by Christiane Amanpour).
* Introduction about how to cite articles and copyright of material on websites.
* TED Talk: Inside the fight against Russia's fake news empire(by Olga Yurkova).
* Detect fake news by checking if what, why, when, how, where, and who are answered.
* Select one claim and make a presentation about it (find evidence against or in support of the claim).

# 2019-10-03

* Reminder about filling the personal information in Google sheet.
* Classmates share their diary to whole class.
* Presentation of mini problem 3: Claim of fake news.
* Videos watching about economy.
* Discussion about critical thinking after watching videos related to economy.
* Domain knowledge is important for learning new topic that would affect our critical thinking.

# 2019-10-17

* Two classmates share their diary to whole class.
* Presentation of mini problem 4: Claim related to economy.
* Videos watching about why does extremism flourish.
* Discussion about critical thinking after watching videos related to extremism flourish.
* It is difficult to find somebody that you think it is undeserving your compassion and give compassion to them because I have stranger anxiety to make communication with unknown people. Any suggestion?

# 2019-10-24

* One classmate shares her diary to whole class. (Reminder: Use bullet points to write diary.)
* How to define knowledge: well-justified true belief.
* Supergroup discussion about today's mini problem presentation. (Claim, Testable Hypothesis, Contradicting/Supporting evidence, agree/disagree)
* Demonstration cortical inhibition by doing experiment: Real-time detection about Red ADC Count and Heart Rate Variability.
* Videos watching about how to live healthy.
* Do exercise 3 to 4 times per week and at least 30 mins per time: Make brain healthy.
* Learn how to come up with claim and generate corresponding hypothesis after watching two videos about how to live healthy.

# 2019-10-31

* One classmate shares his diary to whole class. (Reminder: Use correct format to write diary.)
* Presentation of mini problem: Claim related to healthy life and three healthy rules for living healthy. (Reminder: Check your slides carefully and follow appropriate format.)(Go straight to profession!)
* Check: Claim, Testable Hypothesis, Supporting evidence (convicing?)
* Videos watching about depression.
* Discussion about how to recognise when a friend is depressed, what to say to a depressed friend and how to help a friend with depression. (Key: To be a patient listener!)

# 2019-11-07

* Two classmates share their diary to whole class. (Correct format!)
* Presentation of mini problem: Claim related to depression. (Reminder: Please provide appropriate references/sources.)
* Three classic ways for making presentation: Ethos (credibility, trust or character of the speaker); Pathos(emotional connection to the audience); Logos (logical argument).
* Videos watching about how to choose happiness/fulfilment.
* Discuss about claims that you found in the videos and form testable hypotheses based on them.
* Come up with three tangible rules for how to live a happy and fulfilling life (Give an example).

# 2019-11-14

* One classmate shares his diary to whole class.
* Presentation of mini problem: Claim and Hypothesis; Three tangible rules for how to live a happy and fulfilling life.
* Video watching about how does the legal system work.
* I am not quite familar with the domain knowledge and terms of legal dictionary in English; therefore, it's a little bit difficult for me to get whole ideas in the video.
* Find the law and compare how it is implemented in the U.K. and Taiwan.

# 2019-11-21

* One classmate shares his diary to whole class.
* Don't waste your time.
* Presentation of mini problem: Find the law and compare how it is implemented in the U.K. and France/Taiwan.
* Video watching about how to stay connected and free.
* Debate how to stay connected and free with the aim of reaching a consensus on a minimal set of rules and add the consensus rules (vote by whole classmates) to the presentation next week.

# 2019-11-28

* One classmate shares his diary to whole class.
* Presentation of mini problem and discussion: Stay connected and free with the aim of reaching a consensus on a minimal set of rules and add the consensus rules
* Video watching about how to make sense of events.
* Analyze news sources from different points of view (left/right, different countries), make historical comparisons and look at interest of stakeholders.

# 2019-12-05

* One classmate shares his diary to whole class.
* Presentation of mini problem and discussion: Analyze news sources from different points of view, make historical comparisons and look at interest of stakeholders.
* Video watching about planetary boundaries.
* This is my first time to hear about planetary boundaries.
* Prepare a three minute presentation about the planetary boundary/social foundation assigned to each group and debate next week.

# 2019-12-12

* Give three minute presentation about the planetary boundary/social foundation.
* We have the debate from each group and argue in favour of why their boundary/foundation is the most important one to address right now by giving a recommended action.
* I like today's lecture because we have more discussion and interaction through asking unique question to panel of different boundary/foundation.
* Then have everyone in the class vote three times for boundary/foundation.
* Collect arguments for a debate related to third industrial revolution next week.

# 2019-12-19

* Presentation of mini problem and discussion: Questions related to third industrial revolution.
* We have a debate between capitalists, workers, and environmentalists related to third industrial revolution.
* Video watching about how to success and some useful rules for increase productivity.

## Diary
	## 191219 Thu
	A. Successful and Unproductive
	B. I felt successful because I finished my group presentation of ProfSkills. I felt unproductive because I did not study well for paper work related to my research.
	C. I will study with schedule by making notes.

	## 191220 Fri
	A. Successful and Productive
	B. I felt successful when thinking about my final project proposal. I felt productive because I studied well by making notes.
	C. I will get up tomorrow at 08:00 to join volleyball competition of my nephew.

	## 191221 Sat
	A. Successful and Unproductive
	B. I felt successful because my nephew’s team played volleyball quite well. I felt unproductive because I was so excited about the volleyball competition all day that I did not study at all.
	C. I will discuss with my team member for final project of IMS.

	## 191222 Sun
	A. Unsuccessful and Productive
	B. I felt unsuccessful because I need more paper work for final project of IMS, even though discussing with my team member for suggestions. I felt productive because I managed to find some references for final project of IMS.
	C. I will discuss with my team member for final project of IMS to make sure the methodology.

	## 191223 Mon
	A. Successful and Productive
	B. I felt successful because I finished one case for final project of IMS. I felt productive because I studied well for preparing final project of IMS.
	C. I will send Christmas greetings to teachers and friends.

	## 191224 Tue
	A. Successful and Productive
	B. I felt successful because I sent Christmas greetings to teachers and friends. I felt productive because I replied all the email regarding Christmas greetings.
	C. I will work on reviewing my Indonesian for final exam.

	## 191225 Wed
	A. Unsuccessful and Productive
	B. I felt unsuccessful because I forgot many vocabularies and usages of Indonesian. I felt productive because I started to prepare the final exam of ProfSkills and checked the answer of questions.
	C. I will discuss with my team member for final project of IMS to arrange the time plan schedule of preparing presentation next week.

## 5 rules to be more successful or productive
* Know yourself.
* Create the following days plan.
* Focus on most important tasks first. (Focus on one task at a time.)
* Get better at saying “No.”
* Plan, plan, plan! (review progress)

# 2019-12-26

* Three classmates share their diary about how successful and productive that they felt each day.
* Whole class share: 5 rules to be more successful or productive.
* Presentation of mini problem and discussion: Find evidence related to the question assigned to the group and categorise all sources into trustworthy or questionable and identify the original data source and number of agents that it has gone through.
* Brainstorming to come up with three solutions (one for your assigned question, two for other questions) based on the topics for the final project.
* Write your course diary about how successful and productive you felt each day.

## Diary
	## 191226 Thu
	A. Unsuccessful and Unproductive
	B. I felt unsuccessful because I did not finish my group presentation of ProfSkills. I felt unproductive because I did not write the report well for final project of IMS.
	C. I will arrange my time for writing IMS report and revising paper.

	## 191227 Fri
	A. Successful and Productive
	B. I felt successful because I finished the draft of IMS report. I felt productive because I revised some parts of paper based on reviewer's comments.
	C. I will discuss with my team member for final project of IMS.

	## 191228 Sat
	A. Successful and Unproductive
	B. I felt successful because I knew how to revise IMS report. I felt unproductive because I went home and I did not study at all.
	C. I will get up tomorrow at 09:00 for revising the report of IMS final project.

	## 191229 Sun
	A. Unsuccessful and Productive
	B. I felt unsuccessful because I needed more paper work for final project of IMS and added more contents. I felt productive because I managed to find some useful references for final project of IMS.
	C. I will discuss with my team member for final project of IMS.

	## 191230 Mon
	A. Successful and Productive
	B. I felt successful because I finished the revised report of IMS final project. I felt productive because I studied well for preparing the final project of IMS.
	C. I will make presentation slides of IMS final project.

	## 191231 Tue
	A. Unsuccessful and Productive
	B. I felt unsuccessful because I did not finish the presentation slides of IMS. I felt productive because I cleaned my room.
	C. I will work on preparing my presentation slides of IMS.

	## 200101 Wed
	A. Successful and Productive
	B. I felt successful and productive because I finished the presentation slides of IMS.
	C. I will work on reviewing my final exams next week.

# 2020-01-02
* Classmate shares diary about successful/unsuccessful; productive/unproductive for each day in the previous week.
* Video watching about how to make a change.
* Presentation about three implement solutions related to assigned questions/problems.
* Decide 7 implement solutions for the presentation by making three mins video outside of the classroom via voting.
* I really enjoy today's lecture because we discuss about the implement solutions for different questions/problem.

# 2020-01-09
* Video watching: 1.The mathematics of love. 2.How to fix a broken heart.
* Take final exam.
* Review about course objectives.
* Final report presentation: Watch videos from 7 groups and get feedback to improve video.
* Take lecture evaluation.