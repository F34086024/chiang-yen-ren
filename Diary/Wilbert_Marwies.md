# 2020-09-17#
1. The lecture was great
2. The TED Talk increase my knowledge about the world condition
3. I want to learn more in this course

# 2020-09-24#
1. I learned how to manage a good presentation
2. Learned how to find a fake news through TED Talk

# 2020-10-08#
1. I learned aboout productivity growth, shorterm and longterm cycle
2. transaction is a simple part of a economy
3. money plus credit equal to total spending
4. credit is a crucial part in economy
5. I learned how the economy work

# 2020-10-15#
1. I learned about a dual reality world
2. Money, Religion, nation are fictional things in the world
3. I also learned about facism and racism that happen in this society

# 2020-10-22#
1. I learned about how do some parts od a brain works
2. By doing exeercises, it can boost your mood and help you to increase brain memory
3. Doing exercise can boost the brain to produce more new cells
4. The world is still in sick-care program

# 2020-10-29#
1. Depression is a serious mental heatlh issue
2. Depression can lead to suicide
3. People with mental health problems prefer not to share their issue

# 2020-11-12#
1. Legal fiction is a part of our life since we were borned
2. Legal fiction is an assumption that something occured which, in fact, is not the case, but that is made in the law to enable a court to equitably resolve a matter before it
3. Corporation is fake which means its an entity that is only legal framework
4. Certificate of birth is an indicator which unconsciously make your child became a legal fiction

# 2020-11-26#
1. The Roman was the first culture to have social program
2. In the Roman Empire, it was bread and chariot races and gladiatorial games that filled the belly and distracted the mind, allowing emperors to rule as they saw fit.

# 2020-12-10#
1. Planetary boundaries are important in our life
2. Planetary boundaries is a measure to maintain a global environment that is conducive for human development and well being
3. Each of the planetary boundaries are important equally
4. Some boundaries have exceed the limit and we mankind have to think a way to slove these problems

# 2020-12-31#
1. The reason this course was made is to give  a better future to future generation
2. Future generation might live in dangerous environment if we now keep living badly

# 2021-1-7#

Course Objectives

1. Know the current world demographics
	
	* Suggestion: The current world demographics is much better than the previous century ago. Today, people's life expectancy rate is much higher than before.
	
2. Ability to use demographics to explain engineering needs

	* Suggestion: Demographic analysis is used to study the population based on factors such as age, race, and sex.
	* These data refer to socio-economic information expressed statistically, also including employment, education, income, birth and death rates, etc.
	
3. Understand planetary boundaries and the current state
	
	* Suggestion: We need to know these planetary boundaries so that we won't exceed its limit so that the future generation will not be living in a bad environment.

4. Understand how the current economic system fail to distribute resources
	
	* Suggestion: The current economic system produces an irreconcilable conflict between the goal of creating economically just and environmentally sustainable societies and embracing sustained economic growth. 
	* The current policies are constructed to produce more millionaires and billionaires.

5. Be familiar with future visions and their implications, such as artificial intelligence
	
	* Suggestion: As technology is developing more and more, we also need to adapt to the development so that we will not fall behind. New technologies also open door for many business chances as now evrything is done online.

6. Understand how data and engineering enable a healthier life
	
	* Suggestion: Providers and healthcare systems that master the data and then use it to improve quality of care for better population health and at less cost will benefit from financial incentives.
	
7. know that social relationships gives a 50% increased likelihood of survival
	
	* Suggestion: We as a human being need to socialize. Loneliness can lead one to become depression or other mental health issue.
	
8. Be familiar with depression and mental health issues
	
	* Suggestion: Depression is an illness which we must take it seriously as it can lead to death or suicide. 
	* There are plenty of ways to help one who is depressed such as listening to them.
	
9. Know the optimal algorithm for finding a partner in life
	
	* Suggestion: A partner in life is crucial as he or she can become a motivation or support for you in this life. 
	
10. Develop a custom of questioning claims to avoid "fake news"
	
	* Suggestion: Fake news are really a common things nowadays as social media arise which make fake news easy to appear. We always need to analyze every news we read if it is real or fake.
	* People tend to be polarized by fake news so it is better to have the claim to support every news we read.
	
11. Be able to do basic analysis and interpretation of time series data
	
	* Suggestion: Basic analysis is important for us human so that we will not easily being fooled.
	
12. Experience of collaborative and problem based learning
	
	* Suggestion: One way to increase collaborative and problem based learning is by doing a group project.
	
13. Understand that professional success depends on social skills
	
	* Suggestion: We need to socialize with many people to develop or connection and networking. these things can help us in th efuture in term of doing things such as businesses.
	
14. Know that the culture of the work place affect performance
	
	* Suggestion: People who have a good work environment tend to do better at their work rather than people who work is not supporting environment.
	* I think that CEO or the boss must know how to provide his/her staffs and workers a better work environment which will make them do better at work.